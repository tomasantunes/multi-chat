# Multi-chat

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Y8Y2M1UI)

A chat application template with the option of selecting nicknames.

Icons were provided by Iconsdb.com

![Screenshot](https://i.imgur.com/zBMmtNN.png "Screenshot")